package m2i.Planes;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import m2i.Planes.dao.FlightDao;
import m2i.Planes.dao.PassengerDao;
import m2i.Planes.dao.PiloteDao;
import m2i.Planes.models.Flight;
import m2i.Planes.models.Passenger;
import m2i.Planes.models.Pilote;
import m2i.Planes.utils.HibernateUtil;

public class App {

	public static void main( String[] args ) throws Exception {

		FlightDao flightDao = new FlightDao();
		PiloteDao piloteDao = new PiloteDao();
		PassengerDao passengerDao = new PassengerDao();
		Flight v1, v2;
		Transaction t;
		Pilote p1, p2;
		Passenger pa1;
		
		
		// Utilisation sans HibernateUtil ni DAO //////////////////////////////////////////////////
		System.out.println("> Utilisation sans HibernateUtil ni DAO : recupération d'un vol");
		// Creation de la session factory
		System.out.println("\tCreation de la sessionFactory (HibernateUtil)");
		SessionFactory sf = new Configuration().configure().buildSessionFactory();
		// Creation de la session
		System.out.println("\tCreation de la session");
		Session s = sf.openSession();
		// Appel des méthodes de la session
		System.out.println("\tVol recupere : " + s.get(Flight.class,  8));
		// Fermeture de la session
		System.out.println("\tFermeture de la session");
		s.close();
		
		
		
		
		// Utilisation du DAO /////////////////////////////////////////////////////////////////////
		System.out.println("> Utilisation avec HibernateUtil et DAO : recupération d'un vol");
		// Creation de la session factory /////////////////////////////////////////////////////////
		System.out.println("\tCreation de la sessionFactory (HibernateUtil)");
		HibernateUtil.getSessionFactory();
		// Ouverture d'une session et enregistrement dans le DAO
		System.out.println("\tCreation de la session");
		flightDao.setSession(HibernateUtil.getSessionFactory().openSession());
		// Appel des méthodes du DAO
		System.out.println("\tVol recupere : " + flightDao.findById(8));
		// Fermeture de la session
		System.out.println("\tFermeture de la session");
		flightDao.getSession().close();
		
		
		// Identite des instance persistante //////////////////////////////////////////////////////
		System.out.println("> Identite des instance persistante");
		// Initialisation DAO
		System.out.println("\tCreation de la session");
		flightDao.setSession(HibernateUtil.getSessionFactory().openSession());
		// Recuperation d'un vol
		System.out.println("\tRécuperation d'un vol");
		v1 = flightDao.findById(8);
		// Recuperation du meme vol
		System.out.println("\tRécuperation du meme vol");
		v2 = flightDao.findById(8);
		// Vérification identite
		System.out.println("\tComparaison des deux vols : (" + v1 + " == " + v2 + "). Meme adresse memoire !");
		assert(v1 == v2);
		// Fermeture de la session
		System.out.println("\tFermeture de la session");
		flightDao.getSession().close();
		
		
		// Automatic dirty checking ///////////////////////////////////////////////////////////////
		System.out.println("> Automatic dirty checking");
		// Initialisation DAO
		System.out.println("\tCreation de la session");
		flightDao.setSession(HibernateUtil.getSessionFactory().openSession());
		// Recuperation d'un vol
		System.out.println("\tRécuperation d'un vol");
		v1 = flightDao.findById(8);
		System.out.println("\tNum flight (original) = " + v1.getNum());
		// Modification du vol dans une transaction
		System.out.println("\tOuverture d'une transaction et modification du vol");
		t = flightDao.getSession().beginTransaction();
		v1.setNum(v1.getNum()+1);
		System.out.println("\tNum flight (after modif, in memory) = " + v1.getNum());
		System.out.println("\tFin de la transaction");
		t.commit();
		// Fermeture de la session
		System.out.println("\tFermeture de la session");
		flightDao.getSession().close();
		// Reouverture de la session 
		System.out.println("\tCreation de la session");
		flightDao.setSession(HibernateUtil.getSessionFactory().openSession());
		// Recuperation d'un vol
		System.out.println("\tRécuperation du meme vol");
		v1 = flightDao.findById(8);
		System.out.println("\tNum flight (after modif, from DB) = " + v1.getNum() + ". La modification est bien sauvegardee dans la DB !");
		// Fermeture de la session
		System.out.println("\tFermeture de la session");
		flightDao.getSession().close();
		
		
		// Cascade : opération sur des graphs d'objets ////////////////////////////////////////////
		System.out.println("> Cascade : opération sur des graphs d'objets");
		// Initialisation DAOs
		System.out.println("\tCreation de la session");
		flightDao.setSession(HibernateUtil.getSessionFactory().openSession());
		passengerDao.setSession(flightDao.getSession());
		// Creation d'un passager et d'un vol
		System.out.println("\tCreation d'un vol et d'un passager");
		pa1 = new Passenger();
		pa1.setFirstName("Foo");
		v1 = new Flight();
		v1.setNum(321);
		pa1.addFlights(v1);
		v1.addPassenger(pa1);
		// Enregistrement du pilote (pas de cascadeType) => erreur
		System.out.println("\tEnregistrement d'un graphe d'objet SANS cascadeType.PERSIST");
		try {
			passengerDao.save(pa1);
		} catch (Exception e) {
			System.out.println("\terreur (attendue) : " + e);
		}
		// Enregistrement du vol avec cascadeType.PERSIST) => ok, le pilote est aussi sauvegarde
		System.out.println("\tEnregistrement d'un graphe d'objet AVEC cascadeType.PERSIST");
		flightDao.save(v1);
		System.out.println("\tPas d'erreur ! (et les objets sont enregistrés dans la DB)");
		// Fermeture de la session
		System.out.println("\tFermeture de la session");
		flightDao.getSession().close();

		
		// Fetch : récupération de graphes d'objets ///////////////////////////////////////////////
		System.out.println("> Fetch : récupération de graphes d'objets");
		// Ouverture session
		System.out.println("\tCreation de la session");
		flightDao.setSession(HibernateUtil.getSessionFactory().openSession());
		// Récupération d'un vol
		System.out.println("\tRécupération d'un vol");
		v1 = flightDao.findById(8);
		// Fermeture de la session
		System.out.println("\tFermeture de la session");
		flightDao.getSession().close();
		// Tentative d'accès aux passager du vol => erreur : l'instance est détachée, il n'y a plus de contexte de persistance
		System.out.println("\tAccès aux passagers du vol (hors session)");
		try {
			System.out.println("\t" + v1.getPassengers());
		} catch(Exception e) {
			System.out.println("\terreur (attendue) : " + e);
		}
		// Ouverture session
		System.out.println("\tCreation de la session");
		flightDao.setSession(HibernateUtil.getSessionFactory().openSession());
		// Récupération d'un vol
		System.out.println("\tRéattachement du vol à la nouvelle session");
		v1 = flightDao.save(v1);
		// Tentative d'accès aux passager du vol => ok
		System.out.println("\tAccès aux passagers du vol (dans une session)");
		System.out.println("\t" + v1.getPassengers());
		// Fermeture de la session
		System.out.println("\tFermeture de la session");
		flightDao.getSession().close();
		
	}

}
