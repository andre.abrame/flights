package m2i.Planes.utils;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {
    private static final SessionFactory sessionFactory;
    
    static {
//        try {
            sessionFactory = new Configuration().configure().buildSessionFactory();
//        } catch (HibernateException e) {
//        	throw new PersistenceInitializationException("Error while initiliazing hibernate SessionFactory", e);
//        } catch (Throwable ex) {
//            System.err.println("Initial SessionFactory creation failed." + ex);
//            throw new ExceptionInInitializerError(ex);
//        }
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
}