package m2i.Planes.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;




public class GenericDAO<T> {

	protected Session s;	
	
	private Class<T> clazz;
	
	
	public GenericDAO(Class<T> clazz) {
		this.clazz = clazz;
	}
	
	
	public Session getSession() {
		return s;
	}

	public void setSession(Session s) {
		this.s = s;
	}

	
	public T findById(int id) {
		T ret;
		ret = s.get(clazz , id);
		return ret;
	}

	public List<T> findAll() {
		List<T> ret;
		ret = (List<T>) s.createQuery("from " + clazz.getName()).list();
		return ret;
	}
	
	public T save(T a) throws Exception {
		Transaction tx = null;
		T ret;
		try {
			tx = s.beginTransaction();
			ret = (T) s.merge(a);
			tx.commit();
		} catch (Exception e) {
			if (tx != null)
				tx.rollback();
			throw e;
		}
		return ret;
	}
	
	public void delete(T a) throws Exception {
		System.out.println("removing");
		Transaction tx = null;
		try {
			tx = s.beginTransaction();
			s.delete(a);
			tx.commit();
		} catch (Exception e) {
			if (tx != null)
				tx.rollback();
			throw e;
		}
	}
	
}
