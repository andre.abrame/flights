package m2i.Planes.dao;

import java.util.List;

import javax.persistence.Query;

import m2i.Planes.models.Passenger;

public class PassengerDao extends GenericDAO<Passenger> {

	public PassengerDao() {
		super(Passenger.class);
	}

	public List<Passenger> findByName(String nom, String prenom) {
		List<Passenger> ret;
		Query q = s.createQuery("from Passenger where lastName like :lastname and firstName like :firstname");
		q.setParameter("lastname", "%"+nom+"%");
		q.setParameter("firstname", "%"+prenom+"%");
		ret = q.getResultList();
		return ret;
	}
	
}
