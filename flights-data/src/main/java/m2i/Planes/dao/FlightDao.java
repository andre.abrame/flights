package m2i.Planes.dao;

import java.util.List;

import javax.persistence.EntityExistsException;
import javax.persistence.RollbackException;
import javax.persistence.TransactionRequiredException;

import org.hibernate.Session;
import org.hibernate.Transaction;

import m2i.Planes.models.Flight;


public class FlightDao {

	protected Session s;	
	
	
	public Session getSession() {
		return s;
	}

	public void setSession(Session s) {
		this.s = s;
	}

	
	public Flight findById(int id) {
		Flight ret;
		ret = s.get(Flight.class , id);
		return ret;
	}

	public List<Flight> findAll() {
		List<Flight> ret;
		ret = (List<Flight>) s.createQuery("from Vol").list();
		return ret;
	}
	
	public Flight save(Flight v) throws Exception {
		Flight ret;
		Transaction tx = null;
		try {
			tx = s.beginTransaction();
			ret = (Flight) s.merge(v);
			tx.commit();
		} catch (Exception e) {
			if (tx != null)
				tx.rollback();
			throw e;
		}
		return ret;
	}
	
	public void delete(Flight v) throws Exception {
		Transaction tx = null;
		try {
			tx = s.beginTransaction();
			s.delete(v);
			tx.commit();
		} catch (Exception e) {
			if (tx != null)
				tx.rollback();
			throw e;
		}
	}
}
