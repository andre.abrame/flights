package m2i.Planes.dao;

import m2i.Planes.models.Airport;

public class AirportDao extends GenericDAO<Airport> {

	public AirportDao() {
		super(Airport.class);
	}
	
}
