package m2i.Planes.dao;

import java.util.List;

import javax.persistence.EntityExistsException;
import javax.persistence.RollbackException;
import javax.persistence.TransactionRequiredException;

import org.hibernate.Session;
import org.hibernate.Transaction;

import m2i.Planes.models.Pilote;


public class PiloteDao {

	protected Session s;	
	
	
	public Session getSession() {
		return s;
	}

	public void setSession(Session s) {
		this.s = s;
	}

	
	public Pilote findById(int id) {
		Pilote ret;
		ret = s.get(Pilote.class , id);
		return ret;
	}

	public List<Pilote> findAll() {
		List<Pilote> ret;
		ret = (List<Pilote>) s.createQuery("from Vol").list();
		return ret;
	}
	
	public void save(Pilote v) throws Exception {
		Transaction tx = null;
		try {
			tx = s.beginTransaction();
			s.persist(v);
			tx.commit();
		} catch (Exception e) {
			if (tx != null)
				tx.rollback();
			throw e;
		}
	}
	
	public void delete(Pilote v) throws Exception {
		Transaction tx = null;
		try {
			tx = s.beginTransaction();
			s.delete(v);
			tx.commit();
		} catch (Exception e) {
			if (tx != null)
				tx.rollback();
			throw e;
		}
	}
}
