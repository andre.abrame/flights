package m2i.Planes.models;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("PLANE")
public class Plane extends Flyable {

	@Column
	private int nbMotors;
	
	
	public Plane() {}

	
	public int getNbMotors() {
		return nbMotors;
	}

	public void setNbMotors(int nbMotors) {
		this.nbMotors = nbMotors;
	}
	
}
