package m2i.Planes.models;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public abstract class FlyingPersonel extends Person {

	@Column
	private int numEmployee;

	
	public FlyingPersonel() {}


	public int getNumEmployee() {
		return numEmployee;
	}


	public void setNumEmployee(int numEmployee) {
		this.numEmployee = numEmployee;
	}

}
