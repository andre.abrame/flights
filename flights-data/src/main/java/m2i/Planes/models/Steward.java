package m2i.Planes.models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@Entity
public class Steward extends FlyingPersonel {


	@Column
	private String grade;

	@ManyToMany
	@JoinTable
	private Set<Flight> jeSuisSteward = new HashSet<Flight>();
	
	
	public Steward() {}

	
	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public Set<Flight> getJeSuisSteward() {
		return jeSuisSteward;
	}

	public void setJeSuisSteward(Set<Flight> jeSuisSteward) {
		this.jeSuisSteward = jeSuisSteward;
	}
	
	
}
