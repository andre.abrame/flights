package m2i.Planes.models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Airport {

	@Id
	@GeneratedValue
	private int id;

	@OneToMany(mappedBy="departure")
	private Set<Flight> departures = new HashSet<Flight>();

	@OneToMany(mappedBy="arrival")
	private Set<Flight> arrivals = new HashSet<Flight>();
	
	
	public Airport() {}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Set<Flight> getDepartures() {
		return departures;
	}

	public void setDepartures(Set<Flight> departures) {
		this.departures = departures;
	}

	public Set<Flight> getArrivals() {
		return arrivals;
	}

	public void setArrivals(Set<Flight> arrivals) {
		this.arrivals = arrivals;
	}


	public void addDepartures(Flight v) {
		departures.add(v);
	}

	public void addArrivals(Flight v) {
		arrivals.add(v);
	}
	
}
