package m2i.Planes.models;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Transient;

//@Entity
@Embeddable
public class Passeport {
	
//	@Id
//	@GeneratedValue(generator = "system-foreign")
//	@GenericGenerator(name = "system-foreign", strategy="foreign", parameters = { @Parameter(name = "property",
//	value = "passager") })
//	private int id;
	
	@Column
	private int num;
	
//	@OneToOne(mappedBy="passport")
	@Transient
	private Passenger passager;
	
	
	
	public Passeport() {}

	
	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}


	public Passenger getPassager() {
		return passager;
	}


	public void setPassager(Passenger passager) {
		this.passager = passager;
	}

//
//	public int getId() {
//		return id;
//	}
//
//
//	public void setId(int id) {
//		this.id = id;
//	}
	
	

}
