package m2i.Planes.models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

@Entity
public class Pilote extends FlyingPersonel {

	@Column
	private String licence;

	@OneToMany(mappedBy="pilote")
	private Set<Flight> jeSuisPilote = new HashSet<Flight>();

	@OneToMany(mappedBy="copilote")
	private Set<Flight> jeSuisCopilote = new HashSet<Flight>();
	
	
	public Pilote() {}
	
	
	public String getLicence() {
		return licence;
	}

	public void setLicence(String licence) {
		this.licence = licence;
	}


	public Set<Flight> getJeSuisPilote() {
		return jeSuisPilote;
	}


	public void setJeSuisPilote(Set<Flight> jeSuisPilote) {
		this.jeSuisPilote = jeSuisPilote;
	}

	public Set<Flight> getJeSuisCopilote() {
		return jeSuisCopilote;
	}

	public void setJeSuisCopilote(Set<Flight> jeSuisCopilote) {
		this.jeSuisCopilote = jeSuisCopilote;
	}

	public void addJeSuisPilote(Flight v) {
		jeSuisPilote.add(v);
	}

	public void addJeSuisCopilote(Flight v) {
		jeSuisCopilote.add(v);
	}
	
	
	
}
