package m2i.Planes.models;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("CHOPER")
public class Choper extends Flyable {
	
	@Column
	private boolean canGoOnWater;
	
	
	public Choper() {}


	public boolean isCanGoOnWater() {
		return canGoOnWater;
	}

	public void setCanGoOnWater(boolean canGoOnWater) {
		this.canGoOnWater = canGoOnWater;
	}
	
}
