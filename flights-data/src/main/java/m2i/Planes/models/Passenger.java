package m2i.Planes.models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@Entity
//@DiscriminatorValue("PASSENGER")
public class Passenger extends Person {

	@Embedded
//	@OneToOne
//	@PrimaryKeyJoinColumn
	private Passeport passport;

	
	public Passenger() {}
	

	public Passeport getPassport() {
		return passport;
	}

	public void setPassport(Passeport passport) {
		this.passport = passport;
	}
	

	@ManyToMany
	@JoinTable
	private Set<Flight> flights = new HashSet<Flight>();


	public void addFlights(Flight v) {
		flights.add(v);
	}
	
	
}
