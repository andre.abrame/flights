package m2i.Planes.models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

@Entity
public class Flight {

	@Id
	@GeneratedValue
	private int id;
	
	@Column
	private int num;
	
	@ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	@JoinColumn
	private Pilote pilote;

	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn
	private Pilote copilote;

	@ManyToMany(mappedBy="jeSuisSteward", cascade=CascadeType.ALL)
	private Set<Steward> stewards = new HashSet<Steward>();

	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn
	private Airport departure;

	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn
	private Airport arrival;

	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn
	private Flyable flyable;

	@ManyToMany(mappedBy="flights", cascade=CascadeType.ALL)
	private Set<Passenger> passengers = new HashSet<Passenger>();
	
	
	
	public Flight() {}
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Pilote getPilote() {
		return pilote;
	}

	public void setPilote(Pilote pilote) {
		this.pilote = pilote;
	}

	public Pilote getCopilote() {
		return copilote;
	}

	public void setCopilote(Pilote copilote) {
		this.copilote = copilote;
	}


	public int getNum() {
		return num;
	}


	public void setNum(int num) {
		this.num = num;
	}


	public Set<Steward> getStewards() {
		return stewards;
	}


	public void setStewards(Set<Steward> stewards) {
		this.stewards = stewards;
	}


	public Airport getDeparture() {
		return departure;
	}


	public void setDeparture(Airport departure) {
		this.departure = departure;
	}


	public Airport getArrival() {
		return arrival;
	}


	public void setArrival(Airport arrival) {
		this.arrival = arrival;
	}


	public Flyable getFlyable() {
		return flyable;
	}


	public void setFlyable(Flyable flyable) {
		this.flyable = flyable;
	}


	public Set<Passenger> getPassengers() {
		return passengers;
	}


	public void setPassengers(Set<Passenger> passengers) {
		this.passengers = passengers;
	}
	
	public void addPassenger(Passenger p) {
		passengers.add(p);
	}
	
	
}
