<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Passenger details</title>
</head>
<body>
	<form action="${action}" method="post">
		<label for="prenom">Prenom : </label>
		<input type="text" name="prenom" value="${prenom}"/>
		${prenomError}
		<br/>
		<label for="nom">Nom : </label>
		<input type="text" name="nom" value="${nom}"/>
		${nomError}
		<br/>
		<label for="numPasseport">Numéro de passeport : </label>
		<input type="text" name="numPasseport" value="${numPasseport}"/>
		${numPasseportError}
		<br/>
		<input type="submit" value="Enregistrer"/>
		<br/>
	</form>
	
</body>
</html>