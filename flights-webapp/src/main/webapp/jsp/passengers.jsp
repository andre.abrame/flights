<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Passengers list</title>
</head>
<body>

		<button onclick="window.location.href='passenger/add'">Ajouter un passager</button>
		<br/>
		
	<c:forEach items="${passengers}" var="passenger">
		Prenom : ${passenger.firstName} <br/>
		Nom : ${passenger.lastName} <br/>
		Num passeport : ${passenger.passport.num} <br/>
		<button onclick="window.location.href='passenger?id=${passenger.id}'">Details</button>
		<button onclick="window.location.href='passenger/edit?id=${passenger.id}'">Edit</button>
		<button onclick="window.location.href='passenger/rm?id=${passenger.id}'">Remove</button>
		
		<br/> <br/>
	</c:forEach>
	
</body>
</html>