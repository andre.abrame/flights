package m2i.flights.webapp.controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import m2i.Planes.dao.PassengerDao;
import m2i.Planes.models.Passenger;
import m2i.Planes.utils.HibernateUtil;

@WebServlet("/passengers")
public class PassengersServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private PassengerDao passengerDao;
	

    public PassengersServlet() {
    	passengerDao = new PassengerDao();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// ouverture d'une session hibernate et passage au DAO
		passengerDao.setSession(HibernateUtil.getSessionFactory().openSession());
		// recuperation de la liste des passengers dans le DB
		List<Passenger> lp = passengerDao.findAll();
		// fermeture de la session hibernate
		passengerDao.getSession().close();
		// sauvegarde de la liste des  passengers en attribut de requete
		request.setAttribute("passengers", lp);
		// genere la vue à partir de la JSP
		this.getServletContext().getRequestDispatcher("/jsp/passengers.jsp").forward(request, response);	
	}


}
