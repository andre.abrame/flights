package m2i.flights.webapp.controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import m2i.Planes.dao.PassengerDao;
import m2i.Planes.models.Passenger;
import m2i.Planes.utils.HibernateUtil;

@WebServlet("/passenger")
public class PassengerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private PassengerDao passengerDao;
	

    public PassengerServlet() {
    	passengerDao = new PassengerDao();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// recuperation de l'id passe en parametre de la requete
		int id = Integer.parseInt(request.getParameter("id"));
		// ouverture d'une session hibernate et passage au DAO
		passengerDao.setSession(HibernateUtil.getSessionFactory().openSession());
		// recuperation du passenger dans le DB
		Passenger p = passengerDao.findById(id);
		// fermeture de la session hibernate
		passengerDao.getSession().close();
		// sauvegarde du passenger en attribut de requete
		request.setAttribute("passenger", p);
		// genere la vue à partir de la JSP
		this.getServletContext().getRequestDispatcher("/jsp/passenger.jsp").forward(request, response);	
	}


}
