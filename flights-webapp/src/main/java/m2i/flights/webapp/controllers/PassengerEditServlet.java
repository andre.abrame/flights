package m2i.flights.webapp.controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import m2i.Planes.dao.PassengerDao;
import m2i.Planes.models.Passenger;
import m2i.Planes.models.Passeport;
import m2i.Planes.utils.HibernateUtil;

@WebServlet("/passenger/edit")
public class PassengerEditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private PassengerDao passengerDao;


	public PassengerEditServlet() {
		passengerDao = new PassengerDao();
	}

	private boolean validateParameter(String param) {
		return !param.isEmpty();
	}



	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// recuperation de l'id passe en parametre de la requete
		int id = Integer.parseInt(request.getParameter("id"));
		// ouverture d'une session hibernate et passage au DAO
		passengerDao.setSession(HibernateUtil.getSessionFactory().openSession());
		// recuperation du passenger dans le DB
		Passenger p = passengerDao.findById(id);
		// fermeture de la session hibernate
		passengerDao.getSession().close();
		// Enregistrement des proprietes du passager en attribut de session
		request.setAttribute("nom", p.getLastName());
		request.setAttribute("prenom", p.getFirstName());
		request.setAttribute("numPasseport", (p.getPassport() != null) ? p.getPassport().getNum() : "");
		// enregistre l'action à faire lors de la validation du formulaire en attribut de requete
		request.setAttribute("action", "edit?id="+id);
		// genere la vue à partir de la JSP
		this.getServletContext().getRequestDispatcher("/jsp/passengerAdd.jsp").forward(request, response);	
	}    


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// recuperation de l'id passe en parametre de la requete
		int id = Integer.parseInt(request.getParameter("id"));
		// recuperation des parametres de la requete
		String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");
		String numPasseport = request.getParameter("numPasseport");
		// validation des parametres de la requete un a un 
		if (!validateParameter(nom))
			request.setAttribute("nomError", "Le nom ne peut pas etre vide.");
		if (!validateParameter(prenom))
			request.setAttribute("prenomError", "Le prenom ne peut pas etre vide.");
		if (!validateParameter(numPasseport))
			request.setAttribute("numPasseportError", "Le nmero de passeoprt ne peut pas etre vide.");
		// si la validation echoue sur un des parametres
		if (!validateParameter(nom) ||
				!validateParameter(prenom) ||
				!validateParameter(numPasseport)) {
			// enregistre les valeurs saisies precedemment
			request.setAttribute("nom", nom);
			request.setAttribute("prenom", prenom);
			request.setAttribute("numPasseport", numPasseport);
			// enregistre l'action à faire lors de la validation du formulaire en attribut de requete
			request.setAttribute("action", "edit?id="+id);
			// genere la vue à partir de la JSP
			this.getServletContext().getRequestDispatcher("/jsp/passengerAdd.jsp").forward(request, response);
			// on interrompt l'execution de la fonction
			return;
		}
		// si on est ici, c'est que la validation est reussie !
		// creation nouveau passger
		Passenger p = new Passenger();
		// remplissage des proprietes du passager
		p.setFirstName(prenom);
		p.setLastName(nom);
		p.setPassport(new Passeport());
		p.getPassport().setNum(Integer.parseInt(numPasseport));
		p.getPassport().setPassager(p);
		p.setId(id);
		// ouverture d'une session hibernate et passage au DAO
		passengerDao.setSession(HibernateUtil.getSessionFactory().openSession());
		// sauvegarde du passager dans la DB
		try {
			passengerDao.save(p);
		} catch (Exception e) {
			e.printStackTrace();
		}
		// fermeture de la session hibernate
		passengerDao.getSession().close();
		// redirection vers la liste des passagers
		response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/passengers"));	
	}

}
