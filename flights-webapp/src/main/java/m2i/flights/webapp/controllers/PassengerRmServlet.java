package m2i.flights.webapp.controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Transaction;

import m2i.Planes.dao.PassengerDao;
import m2i.Planes.models.Passenger;
import m2i.Planes.utils.HibernateUtil;

@WebServlet("/passenger/rm")
public class PassengerRmServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private PassengerDao passengerDao;
	

    public PassengerRmServlet() {
    	passengerDao = new PassengerDao();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// recuperation de l'id passe en parametre de la requete
		int id = Integer.parseInt(request.getParameter("id"));
		// ouverture d'une session hibernate et passage au DAO
		passengerDao.setSession(HibernateUtil.getSessionFactory().openSession());
		// suppression du passager
		try {
			passengerDao.delete(passengerDao.findById(id));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// fermeture de la session hibernate
		passengerDao.getSession().close();
		// redirection vers la liste des passagers
		response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/passengers"));	
	}


}
